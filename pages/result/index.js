import React from 'react'
import HomeTemplate from './templates/homeTemplate'

function Home() {
  return (
    <main>
        <HomeTemplate />
    </main>
  );
}

export default Home;
