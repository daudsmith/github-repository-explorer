import React from 'react';
import { withRouter } from 'next/router';
import { connect } from "react-redux";
import { getUserApi } from '../../../redux/actions/userAction';
import styles from '../../../styles/Home.module.css'
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';


const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function HomeTemplate(props) {
    
    return (
        <main>
            <Box sx={{ width: '100%' }}>
              <Stack spacing={3}>
                <Item className={styles.formgroup}>
                  <Stack spacing={3} className={styles.formcontent}>
                  <img
                    src="/icons/github-mark.png"
                    loading="lazy"
                    className={styles.githublogo}
                />
                    <TextField label="Github Username" variant="outlined" size='small' />
                    <Button variant="contained" size='small'>Search</Button>
                  </Stack>
                </Item>
                <Stack direction="row" spacing={2} className={styles.stackcontent}>
                  <Item className={styles.content}>
                    Item 1
                  </Item>
                  <Item className={styles.content}>
                    Item 2
                  </Item>
                  <Item className={styles.content}>
                    Item 3
                  </Item>
                  <Item className={styles.content}>
                    Item 4
                  </Item>
                  <Item className={styles.content}>
                    Item 5
                  </Item>
                </Stack>
              </Stack>
            </Box>
        </main>
    )
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserApi: (newData) => dispatch(getUserApi(newData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(HomeTemplate));