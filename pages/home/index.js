import React, { useState } from 'react';
import { withRouter } from 'next/router';
import { connect } from "react-redux";
import { getUserApi } from '../../redux/actions/userAction';
import styles from '../../styles/Home.module.css'
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import Alert from '@mui/material/Alert';

import { Octokit } from '@octokit/rest';

const octokit = new Octokit();

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


function Home() {
  const [username, setUsername] = useState('');
  const [similarUsernames, setSimilarUsernames] = useState([]);
  const [repos, setRepos] = useState([]);
  const [collapsedItems, setCollapsedItems] = useState(repos);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState('');

  const toggleCollapse = (index) => {
    if (collapsedItems.includes(index)) {
      setCollapsedItems(collapsedItems.filter((item) => item !== index));
    } else {
      setCollapsedItems([...collapsedItems, index]);
    }
  };

  const handleSearch = async () => {
    setIsLoading(true);
    try {
      const response = await octokit.search.users({
        q: username,
      });
      const usernames = response.data.items.map((item) => item.login);
      setSimilarUsernames(usernames.slice(0, 5));
      console.log('this all user with username input',usernames)
      
      const repositories = await Promise.all(
        usernames.slice(0, 5).map(async (username) => {
          const repoResponse = await octokit.repos.listForUser({
            username: username,
          });
          const userRepos = repoResponse.data.map((repo) => ({
            id: repo.id,
            name: repo.name,
            description: repo.description,
            stars: repo.stargazers_count,
          }));
          return { username, repos: userRepos };
        })
      );
      setRepos(repositories);
      setIsLoading(false);
    } catch (error) {
      setIsError(true);
      console.error(error);
    }
  };
    
    return (
        <main>
              <Stack>
                <Item className={styles.formgroup}>
                  <Stack spacing={3} className={styles.formcontent}>
                  <img
                    src="/icons/github-mark.png"
                    loading="lazy"
                    className={styles.githublogo}
                  />
                    <TextField
                      value={username}
                      onChange={(e) => setUsername(e.target.value)}
                      label="Github Username" 
                      variant="outlined"
                      size='small' />
                    <Button variant="contained" size='small' onClick={handleSearch}>
                        {isLoading ? <CircularProgress size={25} color="inherit" /> : 'Search'}
                    </Button>
                  </Stack>
                  {isError ? <Alert severity="error"> {isError} </Alert>: ''}
                </Item>
                  { repos && repos.map ((user, index) => {
                      return (
                        <Stack>
                          <Item key={index} className={styles.stackcontent}>
                            <ListItemButton onClick={() => toggleCollapse(index)}>
                              <ListItemText>{user.username}</ListItemText>
                              {collapsedItems.includes(index) ? <ExpandMore /> : <ExpandLess />}
                            </ListItemButton>
                          </Item>
                          {!collapsedItems.includes(index) && (
                                  user.repos.map((repository, key) => {
                              return <Item key={key} className={styles.content}>
                                            <ListItemButton>
                                              <Typography variant="subtitle1" className={styles.reponame} display="block" gutterBottom>
                                                  {repository.name}
                                              </Typography>
                                              <Typography variant="subtitle1" className={styles.repostars} display="block" gutterBottom>
                                                  {repository.stars} stars
                                              </Typography>
                                            </ListItemButton>
                                            <ListItemButton>
                                              <Typography variant="caption" display="block" gutterBottom>
                                                  {repository.description}
                                              </Typography>
                                            </ListItemButton>
                                      </Item> 
                                      }
                                  )
                              )}
                        </Stack>
                      )
                  })
                }
              </Stack>
      </main>
    )
}

const mapStateToProps = (state) => {
  return {
    userReducer: state.userReducer
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getUserApi: () => dispatch(getUserApi())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));