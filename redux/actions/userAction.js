import { OCTOKIT } from '../../api'
import httpRequest from '../../api/services'

export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const fetchUserBegin = () => ({
  	type: 'FETCH_USER_BEGIN'
});

export const fetchUserSuccess = data => ({
  	type: 'FETCH_USER_SUCCESS',
  	payload: data
});

export const fetchUserFailure = error => ({
  	type: 'FETCH_USER_FAILURE',
  	payload: error
});

export const getUserApi = function() {
	return dispatch => {
		dispatch(fetchUserBegin());
		return httpRequest.get(OCTOKIT).then(function (response) {
	    	dispatch(fetchUserSuccess(response.data));
	    	return response.data;
	    }).catch(error => {
	        dispatch(fetchUserFailure(error.response));
	    	return error.response;
	    });
  	};
}
