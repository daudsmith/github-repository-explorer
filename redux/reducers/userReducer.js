import { FETCH_USER_BEGIN, FETCH_USER_SUCCESS, FETCH_USER_FAILURE } from '../actions/userAction'
  
  const initialState = {
	isFetching: false,
	error: false,
	result: null
  }
  
  const userReducer = ( state = initialState , action ) => {
	switch( action.type ){
	  case 'FETCH_USER_BEGIN':
		state = {...state, isFetching: true, error: false }
		break;
	  case 'FETCH_USER_SUCCESS':
		state = {...state, result: action.payload, isFetching: false, error: false}
		break;
	  case 'FETCH_USER_FAILURE':
		state = {...state, result: action.payload, isFetching: false, error: true}
		break;
	  default:
		break;
	}
	return state
  }
  
  export default userReducer;