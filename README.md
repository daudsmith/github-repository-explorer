# github-repository-explorer



## Summary

This source code was made for recruitment needs at a company, I made it using the Github API library, namely octokit

## Install & Run on your Localhost

```
git clone https://gitlab.com/daudsmith/github-repository-explorer.git
npm install
npm start or npm run dev
```

## Tools

- [React)
- [NextJs)
- [Redux)
- [MaterialUI)
- [Octokit)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

## Authors and acknowledgment
Dade Daud Yusuf

## License
For open source projects, say how it is licensed.


## Notes
I don't use redux in this project because it's not needed and avoids over engineering, but I've prepared a redux framework so if API and state management integration is needed, I can use it right away.
