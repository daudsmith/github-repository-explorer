import axios from 'axios'

const httpRequest = axios.create({
  headers: {
  	'Content-Type': 'application/json'
  },
});

httpRequest.interceptors.request.use(
  function(config) {
    const token = localStorage.getItem('token');
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

httpRequest.interceptors.response.use(function (response) {
    return response;
}, function(error) {
    if (error.response.status == 401) {
      localStorage.removeItem('token:'+token);
      location.reload(true)
      return Promise.reject(error);
    } else {
      return Promise.reject(error);
    }
});

export default httpRequest;