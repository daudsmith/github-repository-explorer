import { Octokit } from '@octokit/rest';
const octokit = new Octokit();

export const OCTOKIT = octokit.search.users;
export const GITHUB_API = `${process.env.NEXT_GITHUB_API_URL}`;